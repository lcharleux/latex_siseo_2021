# Introduction to LaTeX

## SISEO 2021

A repository for the SISEO 2020-2021 LaTeX course.

### Program


* First day: introduction and first steps - come with your documents and laptop.
  - Morning: Examples. Presentation of your documents. Set up on your personal computers. Writing a first document.  - Afternoon: specific elements of a document: figures (diagrams, images, etc.); tables and charts; bibliography; mathematics, algorithms, etc.

* Second day:
  - Slides with Beamer. Scripting with Python. Figures with external tools. Posters. Mathematics processing with external tools. Collaborative writing using GIT, Github and Atom.
  - Afternoon: Practice on your documents (bring an article you would like to reproduce.)

### Check list:

- [x] First article
- [x] Segmentation
- [x] Maths
- [x] Floats
- [x] Tables
- [x] Bibliography
- [x] Other classes (`thesis`, elsarticle, ...)
- [ ] Managing your supervisors/colleagues with GIT
- [ ] Slides with Beamer
- [ ] Posters with BAposter
