import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

x = np.linspace(0., 10., 1000)
y1 = np.cos(x)
y2 = np.exp(-x/8) ** y1

"""
plt.figure()
plt.plot(x, y1)
plt.plot(x, y2)
plt.show()
"""

data = {"x": x, "y1":y1, "y2":y2}
data = pd.DataFrame(data)
data.to_csv("data.csv", index = False)
