import pandas as pd # PANDAS: processes tabular data
from matplotlib import pyplot as plt # MATPLOTLIB: plotting library

data = pd.read_csv("data.csv")

cm2inches = .3937
figsize = ( 18 * cm2inches, 10. * cm2inches )
plt.figure(figsize =figsize)
plt.plot(data.x, data.y1, color = "red", label = "Data 1")
plt.plot(data.x, data.y2, color = "blue", label = "Data 2")
plt.grid()
plt.ylim(-1., 3.)
plt.xlim(0., 10.)
plt.xlabel("Position, $x$ [m]")
plt.ylabel("Amplitude, $y$ [V]")
plt.legend(loc = "best")
plt.savefig("data_plot_python.pdf")
plt.show()
