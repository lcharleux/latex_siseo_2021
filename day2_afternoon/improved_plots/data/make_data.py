import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib
from matplotlib import cm
import matplotlib as mpl

mpl.rcParams["axes.unicode_minus"] = False

def func(x, tau = 1., omega=2.*np.pi):
    return np.exp(-x/tau) * np.cos(omega *x)

data = {}    
x = np.linspace(0., 5., 1000)
pairs = [(1.,2.*np.pi), 
         (2.,1.*np.pi), 
         (5.,.5*np.pi), 
         (10.,.25*np.pi),]
for i in range(len(pairs)):
    t, w = pairs[i]
    y = func(x, t, w)
    data["y{0}".format(i)] = y
    
textwidthcm = 16.99757
cm2inches = .3937
dim = textwidthcm * cm2inches
figsize = (dim, dim)
fig = plt.figure(figsize = figsize)
ax1 = fig.add_subplot(2,1,1) 
for i in range(len(pairs)):
    t, w = pairs[i]
    label = "$\\omega / 2 \\pi = {0}, \\tau = {1}$".format(
            w / 2. / np.pi, t)
    plt.plot(x, data["y{0}".format(i)], label = label)
plt.grid()
plt.xlabel("Position, $x$ [s]")
plt.ylabel("Amplitude, $y$ [V]")
plt.legend(loc = "best")
plt.ylim(-1, 1)

data["x"] = x
data = pd.DataFrame(data)
data.to_csv("data.csv", index = False)

ax2 = fig.add_subplot(2,1,2)

u = np.linspace(-2., 2., 100)
v = np.linspace(-2., 2., 100)
U, V = np.meshgrid(u,v)
W = np.cos(U)**2 * (1+ np.sin(V)**2) * 1/ (U**2 + V**2+1)**.5

plt.contour(U, V,W, 20, colors = "k") 
plt.contourf(U, V,W, 20, cmap = cm.jet) 
cbar = plt.colorbar()
plt.grid()
plt.xlabel("Position, $x$ [s]")
plt.ylabel("Amplitude, $y$ [V]")
cbar.set_label("Some stuff, $u$")
"""
for i in range(len(pairs)):
    t, w = pairs[i]
    label = "$\\omega / 2 \\pi = {0}, \\tau = {1}$".format(
            w / 2. / np.pi, t)
    plt.plot(x, data["y{0}".format(i)], label = label)
plt.grid()
plt.xlabel("Position, $x$ [s]")
plt.ylabel("Amplitude, $y$ [V]")
plt.legend(loc = "best")
"""
plt.tight_layout()
plt.savefig("plot_mpl.pdf")
plt.savefig("plot_mpl.pgf")
#tikzplotlib.save("plot.tex")
tikzplotlib.save(
    "plot.tex",
    axis_height = '\\figH',
    axis_width = '\\figW' 
    )

plt.show()

